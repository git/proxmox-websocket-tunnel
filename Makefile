include /usr/share/dpkg/default.mk

PACKAGE := proxmox-websocket-tunnel
ARCH := $(DEB_BUILD_ARCH)

.PHONY: all
all: check

.PHONY: check
check:
	cargo test --all-features

.PHONY: dinstall
dinstall: deb
	sudo -k dpkg -i build/librust-*.deb

.PHONY: build
build:
	rm -rf build
	rm -f debian/control
	mkdir build
	debcargo package \
	    --config "$(PWD)/debian/debcargo.toml" \
	    --changelog-ready \
	    --no-overlay-write-back \
	    --directory "$(PWD)/build/$(PACKAGE)" \
	    "$(PACKAGE)" \
	    "$$(dpkg-parsechangelog -l "debian/changelog" -SVersion | sed -e 's/-.*//')"
	echo system >build/rust-toolchain
	rm -f build/$(PACKAGE)/Cargo.lock
	find build/$(PACKAGE)/debian -name '*.hint' -delete
	cp build/$(PACKAGE)/debian/control debian/control

.PHONY: dsc
dsc: build
	(cd build/$(PACKAGE) && dpkg-buildpackage -S -uc -us)
	lintian build/*.dsc

.PHONY: deb
deb: build
	(cd build/$(PACKAGE) && dpkg-buildpackage -b -uc -us)
	lintian build/*.deb

.PHONY: clean
clean:
	rm -rf build *.deb *.buildinfo *.changes *.orig.tar.gz
	cargo clean

.PHONY: upload
upload: UPLOAD_DIST ?= $(DEB_DISTRIBUTION)
upload: deb
	cd build; \
	    dcmd --deb rust-$(PACKAGE)_*.changes \
	    | grep -v '.changes$$' \
	    | tar -cf- -T- \
	    | ssh -X repoman@repo.proxmox.com upload --product pve --dist $(UPLOAD_DIST)
